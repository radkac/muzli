<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/Users/radka/weby/webrebelpress/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'rebelpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'b]-0n]:O1>u|fC)mL_pvo36tm48cOWlY&&UFXJyVX>R[YD*P}6xHVbOn g0*dYQG');
define('SECURE_AUTH_KEY',  ' Yq6;l||WM9<7z8{,Q5y-?E@I{^;a.d_,_G5D]{eL8JJM?pfJ$&C[j biD9|P]b^');
define('LOGGED_IN_KEY',    'y;W*uYA|(s_&*?dRo0)8]VZc/n@kGm(1>#6L5t`);HY dK`IKt88V#:/e7{PqP}*');
define('NONCE_KEY',        'B|Wq|Y>4IQ+,gl6O4q)<GJzp--cDt@Mh9n$$q2ri&*K%<JxY7%x4sr?+Kawzq*wA');
define('AUTH_SALT',        '7]gSV_AT*NDMo dm{Xx;?vp-M1Wx8!cf<|J$E?Z8WYN]:P i91cHt%2WH-ssDF~4');
define('SECURE_AUTH_SALT', 't@]~)|`]1/=P~j~Mg=:^3FK~#O|bt_;l3AR{M<o&uV+}cyxYIR83ImO?w%<[W#Z3');
define('LOGGED_IN_SALT',   ')jfxl0hk>P0]?g9(};p`3fd]asSXuRhEB_W.N@-MD^.H|;]w=!CGQ<bY|)B F`L{');
define('NONCE_SALT',       ']o|B_8 7_7,Wv>x9}?EvB@Q[snOmJt|!bi_VrJhXxrV%X^AcMlqf<t>v.?a[XC*b');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
// Enable WP_DEBUG mode
define( 'WP_DEBUG', true );

// Enable Debug logging to the /wp-content/debug.log file
define( 'WP_DEBUG_LOG', true );

// Don't insert break lines in contact form 7
define('WPCF7_AUTOP', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
