# Vzdelávanie Wordpress v mesiaci červen
## Celkový čas: 33:18:33

Graf práce [k náhľadu](https://www.dropbox.com/s/v8100g423omvjnc/moravio_graf_prace_wordpress_cerven.png?dl=0)

* vytiahnutie shortcode do šablóny
* Customizer API – použitie, vytvorenie nových záložiek v Customizeri, zmeny hodnôt
* vlastná Option page – upload loga stránky
* pridanie možnosti na upload loga do Customizeru
* prečistenie Wordpressom generovaného  kódu, pridávanie JS iba tam kde má byť
* rozdelenie kódu z functions.php do logických celkov
* úprava admin menu na mieru, preusporiadanie a vymazanie položiek
* testovanie témy – ako sa to robí, čo všetko treba otestovať kým je téma hotová
* Custom Post Fields – vstavané vo WP, použitie, vytvorenie vlastných polí na zobrazovanie najčítanejších článkov
* vlastné usporiadanie stĺpcov v adminovi, pridanie vlastného stĺpca a zoraďovanie príspevkov podľa meta hodnoty vlastného stĺpca
* skryté meta dáta, pridávanie admin css a js iba na konkrétne podstránky dashboardu
* pravidlá pre rýchly Wordpress – ako optimalizovať kód, debugovanie

## Pluginy

* Duplicator – plugin na migráciu stránky, zálohy a duplikovanie príspevkov
* Query Monitor – plugin na zobrazovanie informácií o queries na stránke, načítavanie skriptov a štýlov, použitie hookov
* Tiny Compress Images – plugin na komprimáciu obrázkov
* WP_Sweep – plugin na zmazanie nepotrebných tabuliek v databáze, zmazanie duplikátov
* Autooptimize – plugin na komprimáciu a minifikáciu CSS, JS, a HTML
* WP Super Cache – plugin na cachovanie stránok

## Vlastné pluginy

* Post View Count – vlastný plugin na zobrazovanie najčítanejších príspevkov, organizácia kódu
* Widget na zobrazenie najčítanejších článkov při zapnutom vytvorenom plugine Post View Count
* Must use Plugin – vytvorenie pluginu s nastaveniami, ktoré užívateľ nemôže vypnúť