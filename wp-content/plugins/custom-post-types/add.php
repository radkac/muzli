<h1>Custom post type</h1>

<h2>Already registered custom post types</h2>
<ul>
<?php
foreach ( get_post_types( '', 'names' ) as $post_type ) {
	echo '<li>' . $post_type . '</li>';
}
?>
</ul>
<hr>
<h2>Add new post type</h2>
<form id="add-type-form" method="post" action="">
	<label for="name">Jméno post type</label>
	<input type="text" name="name">
	
	<label for="public">Bude veřejný?</label>
	<input type="checkbox" name="public">
	
	<label for="archive">Bude mít archiv?</label>
	<input type="checkbox" name="archive">
	
	<button type="submit" name="SubmitButton">Odeslat</button>
</form>

<?php


if(isset($_POST['SubmitButton'])) {
	
	// set right values for inputs
	if($_POST['public'] == 'on')
		$_POST['public'] = 'true';
	else $_POST['public'] = 'false';
	
	if($_POST['archive'] == 'on')
		$_POST['archive'] = 'true';
	else $_POST['archive'] = 'false';
	
	// unset SubmitButton from array
	unset($_POST['SubmitButton']);
	
	// create new object
	$postType = new \Classes\CustomPostType($_POST);
	
	// Gather post data.
	$my_post = array(
			'post_title'    => 'My post2',
			'post_content'  => 'Movie post.',
			'post_status'   => 'publish',
			'post_author'   => 1,
			'post_category' => array( 8,39 ),
			'post_type' => strtolower($postType->getName())
	);
	
	// Insert the post into the database.
	wp_insert_post( $my_post );
}
	
?>
<p><?php echo $postType->getName(); ?></p>
<p><?php echo $postType->getPublic(); ?></p>
<p><?php echo $postType->getArchive(); ?></p>
<?php add_action('init', $postType->createPostType()); ?>



<h2>Already registered custom post types</h2>
<ul>
	<?php
	foreach ( get_post_types( '', 'names' ) as $post_type ) {
		echo '<li>' . $post_type . '</li>';
	}
	?>
</ul>

	