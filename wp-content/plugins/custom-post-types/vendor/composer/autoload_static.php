<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite153a49a1bb54dcf7f4d7674efa1af5a
{
    public static $prefixLengthsPsr4 = array (
        'C' => 
        array (
            'Composer\\Installers\\' => 20,
            'Classes\\' => 8,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Composer\\Installers\\' => 
        array (
            0 => __DIR__ . '/..' . '/composer/installers/src/Composer/Installers',
        ),
        'Classes\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/Classes',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite153a49a1bb54dcf7f4d7674efa1af5a::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite153a49a1bb54dcf7f4d7674efa1af5a::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
