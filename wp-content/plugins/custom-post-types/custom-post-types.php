<?php

/** Plugin Name: CUSTOM POST TYPES
Description: Plugin for custom post types
Author: Radka
*/


	$autoloader = require_once('vendor/autoload.php');
	
	// Use the autoload function to setup our class mapping
	$autoloader->add('Classes\\', __DIR__ . '/src/Classes');

/**
 * Add menu item to admin menu
 */
add_action( 'admin_menu', 'cpt_add_item_to_menu' );

/**
 * Register a custom menu page.
 */
function cpt_add_item_to_menu() {
	add_menu_page(
			__( 'Custom post types', 'textdomain' ),
			'Custom post types',
			'manage_options',
			'custom-post-types/add.php',
			'',
			plugins_url( 'custom-post-types/images/icon.png' ),
			1
	);
}

function createPostType() {
	$labels = array (
			'name' => __('movies'),
			'singular_name' => __('movie')
	);
	$args = array (
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'query_var' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'movies'),
	);
	
	register_post_type( 'movies', $args);
}

add_action('init', 'createPostType');







