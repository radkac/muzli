<?php

namespace Classes;

	class CustomPostType {
		
		private $args = [];
		private $name;
		private $public;
		private $archive;
		
		public function __construct($args) {
			$this->args = $args;
			$this->name = $args['name'];
			$this->public = $args['public'];
			$this->archive = $args['archive'];
		}
		
		/* GETTERS AND SETTERS */
		
		/**
		 * @return array
		 */
		public function getArgs()
		{
			return $this->args;
		}
		
		/**
		 * @return boolean
		 */
		public function getName()
		{
			return $this->name;
		}
		
		/**
		 * @param boolean $name
		 */
		public function setName($name) {
			$this->name = $name;
		}
		
		/**
		 * @return boolean
		 */
		public function getPublic() {
			return $this->public;
		}
		
		/**
		 * @param boolean $public
		 */
		public function setPublic($public) {
			$this->public = $public;
		}
		
		/**
		 * @return boolean
		 */
		public function getArchive() {
			return $this->archive;
		}
		
		/**
		 * @param boolean $archive
		 */
		public function setArchive($archive) {
			$this->archive = $archive;
		}
		
		/* END GETTERS AND SETTERS */
		
		
		public function getList() {
			
		}
		
		public function createPostType() {
			$labels = array (
					'name' => __($this->name),
					'singular_name' => __($this->name)
			);
			$args = array (
					'labels' => $labels,
					'public' => $this->public,
					'publicly_queryable' => true,
					'query_var' => true,
					'has_archive' => $this->archive,
					'rewrite' => array('slug' => strtolower($this->name))
			);
			
			 register_post_type( strtolower($this->name), $args);
		}
	}