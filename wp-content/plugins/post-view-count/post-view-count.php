<?php

	/**
	 * Plugin Name: Post view count
	 * Description: Plugin for custom post types
	 * Author: Radka
	 */

define( 'PVC_PLUGIN', __FILE__ );
define( 'PVC_PLUGIN_DIR', untrailingslashit(dirname(PVC_PLUGIN)) );
define( 'PVC_PLUGIN_URL', untrailingslashit(plugin_dir_url(PVC_PLUGIN)) );


/**
 * INCLUDES
 */
require_once PVC_PLUGIN_DIR . '/includes/functions.php';

/**
 * WIDGET
 */
require_once PVC_PLUGIN_DIR . '/includes/widget.php';

/**
 * ADMIN CODE
 */
if(is_admin()) {
	require_once PVC_PLUGIN_DIR . '/admin/admin.php';
	
}

/**
 * add plus 1 when on single page
 */
add_action('the_content', 'pvc_save_post_view_count');

function pvc_save_post_view_count($content) {
	
	if(is_single()) {
		add_post_views();
	}
	
	return $content;
}

