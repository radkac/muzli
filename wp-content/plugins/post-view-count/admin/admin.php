<?php

/**
 * ADMIN SCRIPTS AND STYLES
 */

add_action('admin_enqueue_scripts', 'muzli_admin_scripts');

function muzli_admin_scripts() {
	wp_enqueue_style('muzli-admin-style', PVC_PLUGIN_URL . '/admin/admin.css'	);
}

/**
 * Functions for post view count in admin
 */

add_filter('manage_posts_columns', 'pvc_add_post_views_columns');
function pvc_add_post_views_columns($columns) {
	$columns['views'] = 'Views';
	
	return $columns;
}

/**
 * populate the 'views' column
 */

add_action('manage_posts_custom_column', 'pvc_add_post_views_columns_data', 10, 2);
function pvc_add_post_views_columns_data($column, $post_id) {
	
	if($column === 'views')
	{
		echo get_post_views($post_id);
	}
	$columns['views'] = 'Views';
	
	return $columns;
}

//make the 'views' sortable
add_filter('manage_edit-post_sortable_columns', 'pvc_add_sortable_views_column');
function pvc_add_sortable_views_column($columns) {
	$columns['views'] = 'views';
	return $columns;
}

//order by views column
add_action('pre_get_posts', 'pvc_views_column_orderby');
function pvc_views_column_orderby($query) {
	if(! is_admin()) {
		return;
	}
	
	$orderby = $query->get('orderby');
	
	if('views' === $orderby) {
		$query->set('meta_key', '_post_view_count');
		$query->set('orderby', 'meta_value_num');
	}
}