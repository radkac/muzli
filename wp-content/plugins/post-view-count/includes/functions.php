<?php

/**
 * Functions for post view count in front
 */
if(! function_exists('get_post_views')) :
	
	/**
	 * Get post view count
	 *
	 * @param bool $post_id
	 *
	 * @return int
	 */
	function get_post_views($post_id = FALSE) {
		if(! $post_id) {
			$post_id = get_the_ID();
		}
		
		$field = '_post_view_count';
		$count = get_post_meta($post_id, $field, TRUE);
		
		if(! $count) {
			// if is in post_view_count weird value
			delete_post_meta($post_id, $field);
			add_post_meta($post_id, $field, '0');
			
			return 0;
		}
		
		return (int)$count;
	}

endif;

if(! function_exists('add_post_views')) :
	
	/**
	 * Add post view count
	 *
	 * @param bool $post_id
	 *
	 * @return int
	 */
	function add_post_views($post_id = FALSE) {
		if(! $post_id) {
			$post_id = get_the_ID();
		}
		
		$field = '_post_view_count';
		$count = get_post_meta($post_id, $field, TRUE);
		
		if(! $count) {
			$count = 0;
			// if is in post_view_count come weird value
			delete_post_meta($post_id, $field);
			add_post_meta($post_id, $field, $count);
		}
		
		(int)$count++;
		update_post_meta($post_id, $field, $count);
		
		return $count;
	}

endif;