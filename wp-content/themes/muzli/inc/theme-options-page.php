<?php

//option page CSS
add_action('admin_print_styles-settings_page_theme_settings', 'muzli_theme_settings_css');
function muzli_theme_settings_css() {
	wp_enqueue_style('muzli-option-style', get_template_directory_uri() . '/css/options-page.css'	);
}

/**
 * Theme Settings
 */

add_action( 'admin_menu', 'muzli_add_admin_menu' );
add_action( 'admin_init', 'muzli_settings_init' );


function muzli_add_admin_menu() {
	add_options_page( 'Theme Settings', 'Theme Settings', 'manage_options', 'theme_settings', 'muzli_options_page' );
}


function muzli_settings_init() {
	
	register_setting( 'muzli_theme', 'muzli_settings', 'save_muzli_theme_settings' );
	
	/**
	 * Section copyright
	 */
	add_settings_section(
			'muzli_copyright_section',
			__( 'Copyright info', 'muzli' ),
			false,
			'muzli_theme'
	);
	
	add_settings_field(
			'copyright by',
			__( 'Copyright by', 'muzli' ),
			'copyright_by_render',
			'muzli_theme',
			'muzli_copyright_section'
	);
	
	add_settings_field(
			'copyright text',
			__( 'Copyright text', 'muzli' ),
			'copyright_text_render',
			'muzli_theme',
			'muzli_copyright_section'
	);
	
	/**
	 * Section logo
	 */
	add_settings_section(
			'muzli_logo_section',
			__( 'Upload a logo', 'muzli' ),
			false,
			'muzli_theme'
	);
	
	add_settings_field(
			'logo',
			__( 'Chose an Image', 'muzli' ),
			'muzli_logo_render',
			'muzli_theme',
			'muzli_logo_section'
	);
}

function save_muzli_theme_settings($data)
{
	
	$data = array_map('sanitize_text_field', $data);
	$options = extend_array(get_option('muzli_settings'), $data);
	
	if(!empty($_FILES['logo']['tmp_name']) && file_is_displayable_image($_FILES['logo']['tmp_name'])) {
		
		$upload = wp_handle_upload($_FILES['logo']);
		echo '<pre>';
		print_r( $upload );
		echo '</pre>';
		$options['logo'] = $upload['url'];
	}
	
	return $options;
}


function copyright_by_render() {
	$options = get_option( 'muzli_settings' );
	$value = isset($options['copyright_by']) ? $options['copyright_by'] : '';
	?>
	<input type='text' name='muzli_settings[copyright_by]' value='<?php echo $value ?>' class="regular-text">
	<?php
}

function copyright_text_render() {
	$options = get_option( 'muzli_settings' );
	$value = isset($options['copyright_text']) ? $options['copyright_text'] : '';
	?>
	<textarea name="muzli_settings[copyright_text]" cols="46" rows="3"><?php echo $value ?></textarea>
	<?php
}

function muzli_logo_render() {
	$options = get_option( 'muzli_settings' );
	$logo = isset($options['logo']) ? $options['logo'] : ''; ?>
	
	<p><input type="file" name="logo"></p>
	
	<?php if($logo) { ?>
		<p><img src="<?php echo esc_url($logo) ?>" alt="muzli-logo" class="muzli-logo"></p>
		<?php
	}
}

function muzli_options_page() {
	
	?>
	<div class="wrap">
		<h1>Theme Settings</h1>
		<form action="options.php" method="post" enctype="multipart/form-data">
			
			<?php
			settings_fields( 'muzli_theme' );
			do_settings_sections( 'muzli_theme' );
			submit_button();
			?>
		</form>
	</div>
	
	<?php
	echo '<pre>';
	print_r( get_option('muzli_settings') );
	echo '</pre>';
}