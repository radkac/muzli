<?php

/**
 * Customizer
 * @param $wp_customize
 */
function muzli_customize_register($wp_customize)
{
	$wp_customize->add_section('copyright', array(
			'title' => 'Copyright',
			'priority' => 30,
			'description' => 'copy info usually in footer'
	));
	
	$wp_customize->add_setting( 'copy_by', array(
			'type' => 'theme_mod',
			'default' => get_option('blogname'),
			'transport' => 'refresh', // or postMessage
			'sanitize_callback' => function ($content) {
				return sanitize_text_field($content);
			}
	) );
	
	$wp_customize->add_setting( 'copy_text', array(
			'type' => 'theme_mod',
			'default' => 'Created by love',
			'transport' => 'refresh', // or postMessage
			'sanitize_callback' => function ($content)
			{
				return wp_kses($content, array(
						'strong' => array(),
						'a' => array(
								'href' => array(),
								'title' => array(),
						),
				));
			},
	) );
	
	$wp_customize->add_control( 'copy_by', array(
			'type' => 'text',
			'priority' => 10, // Within the section.
			'section' => 'copyright', // Required, core or custom.
			'label' => 'Copyright by',
	) );
	
	$wp_customize->add_control( 'copy_text', array(
			'type' => 'textarea',
			'priority' => 20, // Within the section.
			'section' => 'copyright', // Required, core or custom.
			'label' => 'Copyright text',
	) );
}
add_action('customize_register', 'muzli_customize_register');
