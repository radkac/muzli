<?php

add_action('after_setup_theme', 'muzli_setup');
function muzli_setup() {
	/**
	 * Theme support
	 */
	add_theme_support('menus');
	add_theme_support('title-tag');
	add_theme_support('post-thumbnails');
	add_theme_support('automatic-feed-links');
	add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));
	add_theme_support('custom-logo', array(
			'height' => 500,
			'width' => 500,
			'flex-height' => false,
			'flex-width' => false,
			'header-text' => array('site_title', 'site_description'),
	));
	
	/**
	 * Menus
	 */
	
	register_nav_menus(array('primary' => 'Primary Menu'));
	
	/**
	 * Editor style
	 */
	add_editor_style('css/editor-style.css');
	
}

/**
 * Add specific css class by filter
 */
add_filter('body_class', 'muzli_class_names');

function muzli_class_names($classes) {
	$new_class = false;
	
	if(is_page()){
		$new_class = get_post_field('post_name', get_post() );
	}
	
	if(is_single()){
		$new_class = 'blog';
	}
	
	$classes[] = $new_class;
	
	return $classes;
}

/**
 * Add custom styles to the head of login page
 */
add_action('login_head', 'muzli_custom_login');

function muzli_custom_login() {
	echo '<link rel="stylesheet" href="' . THEME_DIRECTORY_URI . '/css/custom-login.css">';
	
	if (has_custom_logo()) {
		$logo = wp_get_attachment_image_url(get_theme_mod('custom_logo'), 'thumbnail');
	?>

			<style>
				.login h1 a {
					background-image: url("<?php echo esc_url($logo) ?>");
				}
			</style>
			
<?php
	}
}

/**
 * Rewrite generated link into wordpress page by my link to homepage
 */
add_filter('login_headerurl', 'muzli_login_header_url');

function muzli_login_header_url() {
	
	return get_home_url();
}