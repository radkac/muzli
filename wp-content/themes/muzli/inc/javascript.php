<?php

add_action('wp_enqueue_scripts', 'add_theme_scripts');

//zavola funkciu add_theme_scripts v momente ked prebieha wp_enqueue_scripts
function add_theme_scripts() {
	
	wp_enqueue_script(
			'muzli-app', // handle mnou vymysleny
			THEME_DIRECTORY_URI . '/js/app.js',    // cesta k suboru
			array('jquery'),   //dependences, ktore su nutne potreba
			null, // verzia
			true // ak je true script sa vlozi do paty, ak je false tak do hlavicky
	);
	
	wp_enqueue_style(
			'muzli-style', get_stylesheet_uri()
	);
	
	wp_enqueue_style(
			'muzli-animations', THEME_DIRECTORY_URI . '/css/animations.css'
	);
	
	wp_enqueue_style(
			'muzli-fonts', THEME_DIRECTORY_URI . '/css/fonts.css'
	);
}