<?php

define( 'THEME_DIRECTORY', get_template_directory() );
define( 'THEME_DIRECTORY_URI', get_template_directory_uri() );

/**
 * Theme Support and Site Settings
 */
require_once THEME_DIRECTORY . '/inc/site-settings.php';

/**
 * Register Sidebars
 */
require_once THEME_DIRECTORY . '/inc/sidebars-widgets.php';

/**
 * Scripts and Styles
 */
require_once THEME_DIRECTORY . '/inc/javascript.php';

/**
 * Customizer
 */
require_once THEME_DIRECTORY . '/inc/customizer.php';

/**
 * Theme Options Page
 */
require_once THEME_DIRECTORY . '/inc/theme-options-page.php';