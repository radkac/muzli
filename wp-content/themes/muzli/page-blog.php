<?php get_header() ?>
			<div class="post-list">
				<?php

					$posts = get_posts(array(
						'posts_per_page' => get_option('posts_per_page'),
						'suppress_filters' => true,
						'meta_key' => '_post_view_count',
						'orderby' => 'meta_value',
						'order' => 'DESC'
					));

					foreach ( $posts as $post ) : ?>

						<article class="post" id="post-<?php echo esc_attr( $post->ID ) ?>">
							<h1 class="post-title">
								<a href="<?php echo esc_url(get_permalink($post))?>">
									<?php echo apply_filters( 'the_title', $post->post_title ) ?>
								</a>
							</h1>
							<?php echo apply_filters( 'the_content', $post->post_content ) ?>
						</article>

					<?php endforeach ?>

					<?php echo do_shortcode('[button color="yellow" link="archive"]Older articles[/button]') ?>
				</div>

<?php get_footer() ?>