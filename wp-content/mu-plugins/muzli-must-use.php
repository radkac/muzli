<?php

/**
 * Plugin Name: Muzli Must Use
 * Description: Wordpress must use plugin
 * Author: Radka
 */

/**
 * SHORTCODES
 */

require_once WPMU_PLUGIN_DIR . '/inc/shortcodes.php';


/**
 * SCRIPTS AND STYLES
 */

require_once WPMU_PLUGIN_DIR . '/inc/javascript.php';

/**
 * CLEANUP REORDER
 */

require_once WPMU_PLUGIN_DIR . '/inc/cleanup-reorder.php';



if ( ! function_exists( 'log_me' ) ) :
	/**
	 * Simple error logging
	 *
	 * @param $message
	 * @return bool
	 */
	function log_me( $message )
	{
		if ( true !== WP_DEBUG ) return false;
		
		if ( is_array($message) || is_object($message) ) {
			return error_log( json_encode($message) );
		}
		
		return error_log( $message );
	}

endif;


if ( ! function_exists( 'extend_array' ) ) :
	
	/**
	 * jQuery style array extend
	 *
	 * @return array
	 */
	function extend_array()
	{
		$args     = func_get_args();
		$extended = array();
		
		if ( is_array( $args ) && count( $args ) )
		{
			foreach ( $args as $array )
			{
				if ( ! is_array( $array ) )	continue;
				$extended = array_merge( $extended, $array );
			}
		}
		
		return $extended;
	}

endif;
