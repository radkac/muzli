<?php

// [button] shortcode
function muzli_button_shortcode( $atts, $content = 'Enter text')
{
	// set defaults
	$atts = shortcode_atts(array(
			'color' => 'red',
			'link' => '#',
	), $atts);
	
	// create css class from color
	if($atts['color'])
		$atts['color'] = 'btn-' . $atts['color'];
	
	$parsed = wp_parse_url($atts['link']);
	if(!isset($parsed['scheme']))
	{
		$atts['link'] = home_url($atts['link']);
	}
	
	// create the html
	$html = '<a href="' . esc_attr($atts['link']) . '" class="btn ' . esc_attr($atts['color']) . ' animate">';
	$html .= esc_html($content);
	$html .= '</a>';
	
	return $html;
}
add_shortcode('button', 'muzli_button_shortcode' );

// [simple_gallery] shortcode
function muzli_gallery_shortcode( $atts)
{
	// set defaults
	$atts = shortcode_atts(array(
			'gallery_class' => 'image-grid group',
			'img_class' => 'gallery-img',
	), $atts);
	
	$media = get_attached_media('image');
	if(! $media)
		return '';
	
	$html = '<div class="' . esc_attr($atts['gallery_class']) . '">';
	foreach($media as $img)
	{
		$html .= '<img src="' . esc_url(wp_get_attachment_image_url($img->ID, 'full')) . '"
					   class="' . esc_attr( $atts['img_class']) . '"
					   alt="' . esc_attr( $img->post_title ) . '">';
	}
	
	$html .= '</div>';
	
	return $html;
}
add_shortcode('simple_gallery', 'muzli_gallery_shortcode' );

function muzli_blog_shortcode($atts)
{
	$posts = get_posts(array(
			'posts_per_page' => get_option('posts_per_page'),
			'suppress_filters' => true,
	));
	
	$html = '<div class="post-list">';
	foreach($posts as $post)
	{
		$html .= '<h1 class="post-title">' . apply_filters('the_title', $post->post_title) . '</h1>';
		$html .= apply_filters('the_content', $post->post_content);
		echo $html;
	}
	$htmlFooter = '</div>';
	$htmlFooter .= do_shortcode('[button color="yellow" link="archive"]Older posts[/button]');
	
	echo $htmlFooter;
}
add_shortcode('blog', 'muzli_blog_shortcode' );

