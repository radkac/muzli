<?php

// stop loading contact form 7 scripts and styles on every page
add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );


add_action('wp_enqueue_scripts', 'muzli_mu_plugin_scripts');

// call function add_theme_scripts at the moment when wp_enqueue_scripts are loading
function muzli_mu_plugin_scripts() {
	
	// load contact-form plugin scripts and styles only on page contact
	if(is_page('contact'))
	{
		if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
			wpcf7_enqueue_scripts();
		}
		
		if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
			wpcf7_enqueue_styles();
		}
	}
}