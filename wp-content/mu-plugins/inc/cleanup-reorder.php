<?php

/**
 * Disable garbage
 */
add_action('init', 'muzli_disable_garbage', 9999);
function muzli_disable_garbage() {
	
	/**
	 * Disable Emojis
	 */
	
	// all actions related to emojis
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	
	// filter to remove TinyMCE emojis
	add_filter( 'tiny_mce_plugins', 'muzli_disable_emojicons_tinymce' );
	
	
	/**
	 * Disable Embed
	 */
	
	remove_action('rest_api_init', 'wp_oembed_register_route');
	remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);
	remove_action('wp_head', 'wp_oembed_add_discovery_links');
	remove_action('wp_head', 'wp_oembed_add_host_js');
	
	
	// clean up wp_head
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('do_feed_rdf', 'do_feed_rdf', 10, 1);
	remove_action('do_feed_rss', 'do_feed_rss', 10, 1);
	//remove_action('do_feed_rss2', 'do_feed_rss2', 10, 1);
	//remove_action('do_feed_atom', 'do_feed_atom', 10, 1);
	remove_action('wp_head', 'feed_links_extra', 3 );
	remove_action('wp_head', 'feed_links', 2 );
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
	remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
	remove_action('wp_head', 'noindex', 1);
	remove_action('wp_head', 'rel_canonical');
	
	//remove Wordpress version from RSS feed
	add_filter('the_generator', '__return_false');
	
	//disable Admin toolbar
	//add_filter( 'show_admin_bar' , 'my_function_admin_bar');
}

function muzli_disable_emojicons_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * Edit Admin Dashboard
 */
add_action('admin_menu', 'muzli_edit_admin_menus', 999);
function muzli_edit_admin_menus()
{
	remove_menu_page('edit-comments.php');
	remove_submenu_page('themes.php', 'theme-editor.php');
	remove_submenu_page('plugin.php', 'plugin-editor.php');
}

/**
 * Reorder Dashboard pages
 */
add_filter('custom_menu_order', 'muzli_custom_menu_order');
add_filter('menu_order', 'muzli_custom_menu_order');

function muzli_custom_menu_order($__return_true) {
	return array(
			'index.php', //Dashboard
			'separator1', // --space--
			'edit.php?post_type=page', //Pages
			'edit.php', //Posts
			'upload.php', //Media
			'themes.php', //Appearance
			'users.php', //Users
			'plugins.php', //Plugins
			'tools.php', //Tools
			'options-general.php', //Settings
	);
}

/**
 * Show second row of editor in admin
 */
add_filter('tiny_mce_before_init', 'muzli_unhide_kitchensink');

function muzli_unhide_kitchensink($args) {
	$args['wordpress_adv_hidden'] = false;
	
	return $args;
}